#!/usr/bin/env bash

for i in `seq 1 10`
do
    echo "-------------------------------------------"
    echo "Running round #$i:"
    wrk -c 10 -t 4 -d10s http://localhost:9050/nginx/people.json
done
