#!/usr/bin/env bash

function configure_repositories() {
  sudo apt-get install wget curl vim -yq
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
  echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
  sudo apt-get update
}

function makes_dir_writeable() {
  sudo chmod -R 777 /usr/share/nginx/html
  sudo chmod -R 777 /etc/tyk
}

function install_nginx() {
  sudo apt-get install nginx -yq
  sudo service nginx start
}

function install_mongodb() {
  sudo apt-get install mongodb-org -yq
  sudo service mongod start
}

function install_redis() {
  sudo apt-get install redis-server -yq
  sudo service redis-server start
}

function install_tyk() {
  wget -q --content-disposition http://bit.ly/1x9In3J
  sudo dpkg -i tyk.linux.amd64_1.1-1_all.deb
}

function install_tyk_upstart() {
  sudo tee /etc/init.d/tyk <<EOF
#! /bin/sh
#
### BEGIN INIT INFO
# Provides: tyk
# Required-Start: \$network
# Required-Stop:  \$network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short Description: Startup script for Tyk
# Description: Script for starting Tyk as a daemon on system startup
### END INIT INFO

set -e

. /lib/lsb/init-functions

CONFIG_DIRECTORY=/etc/tyk

NAME=tyk
TYK_EXEC=/usr/bin/tyk
DAEMON_HOME=/etc/tyk
PID_FILE=/var/run/tyk.pid
START_ARGS="--start --quiet --oknodo --make-pidfile --pidfile \${PID_FILE} --background"

start_tyk()
{
  rm -rf \$PID_FILE
  start-stop-daemon \$START_ARGS --exec \$TYK_EXEC
  log_progress_msg "started"
}

stop_tyk()
{
  start-stop-daemon -p \$PID_FILE --stop --retry 5
  log_progress_msg "stopped"
}

cd \$DAEMON_HOME

if [ \$? -ne 0 ]; then
  echo "Unable to find \$NAME's directory."
  exit 1
fi

case "\$1" in
  start)
    log_daemon_msg "Starting \$NAME"
    start_tyk
    log_end_msg 0
    ;;
  stop)
    log_daemon_msg "Stopping \$NAME"
    stop_tyk
    log_end_msg 0
    ;;
  restart)
    log_daemon_msg "Restarting \$NAME"
    stop_tyk
    start_tyk
    log_end_msg 0
    ;;

  *)
    echo "Usage: /etc/init.d/\$NAME {start|stop|restart}"
    exit 1
esac
EOF
  sudo chmod +x /etc/init.d/tyk
}

configure_repositories
install_nginx
install_redis
install_mongodb
install_tyk
install_tyk_upstart
makes_dir_writeable
