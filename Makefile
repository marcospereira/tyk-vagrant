clean:
	@vagrant destroy -f

install:
	@vagrant up
	@vagrant ssh -- sudo service tyk start

check:
	@curl -I http://127.0.0.1:9080/people.json -s | grep -q 200 && echo "Nginx status:  OK" || echo "Nginx status:  FAIL"
	@curl -I http://127.0.0.1:9050/nginx/people.json -s | grep -q 200 && echo "Repose status: OK" || echo "Repose status: FAIL"

setup: clean install check

perf:
	@./scripts/perf.sh
